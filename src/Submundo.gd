extends Control

signal life

onready var coins = $PlayerInfo/Coins
onready var hearts = $PlayerInfo/PlayerHealth

func _ready():
	$PlayerInfo/Level.text = str(Global.player_level)
	$PlayerInfo/XP.max_value = 10*Global.player_level
	$PlayerInfo/XP.value = Global.xp
	_set_prices()

func _set_prices():
	if Global.player_hearts == Global.MAX_HEARTS:
		$Store/Heart.disabled = true
	if Global.max_jumps == 2:
		$Store/Jump.disabled = true
	$Store/Heart.set_price(10)
	$Store/Damage.set_price(Global.random.randi_range(1,5),Global.player_level)
	$Store/Jump.set_price(20)

func _on_Reviver_pressed():
	Global.revive()
	emit_signal("life")

func _buy(price):
	Global.coins -= price
	coins.coin_update()
	for i in $Store.get_children():
		i.can_buy()

func add_heart(price):
	Global.add_heart()
	_buy(price)
	hearts.heart_update()

func add_strenght(price):
	Global.player_strenght += 1
	_buy(price)

func add_jump(price):
	if Global.max_jumps < 2:
		Global.max_jumps += 1
		_buy(price)
