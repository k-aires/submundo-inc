extends Node2D

const MAX_JUMP = 4

export(int) var max_width = 100
export(int) var max_height = 50
export(float) var max_caves = 0.2

var min_height = (max_height/2)
var cave_space = int(max_width*(max_height/2)*max_caves)

onready var tilemap = $Terra

func clear_tile(click: Vector2):
	click -= position
	click = tilemap.world_to_map(click)
	Global.map[-click.y][click.x] = 0
	tilemap.set_cell(click.x,click.y,-1)
	tilemap.update_bitmask_area(click)

func generate_map():
	if Global.map.size() == 0:
		position.x=-max_width/2*8
		position.y=max_height*8
		_generate_all()
		Global.map_height = position.y
	else:
		position.x = -Global.map[0].size()/2*8
		position.y = Global.map_height
		_populate_tilemap()

func _generate_all():
	tilemap.clear()
	
	# Populate matrix with tiles
	for y in range(0,max_height):
		Global.map.append([])
		Global.map[y].resize(max_width)
		for x in range(0,max_width):
			Global.map[y][x] = 1
	
	_generate_terrain()
	_generate_caves()
	
	_populate_tilemap()

func _generate_terrain():
	# Creat auxiliary vars
	var next_ground = 0
	var direction = 0
	var direction_time = 0
	var height = max_height/2+1
	
	for x in range(0,max_width):
		if direction_time == 0:
			direction_time = Global.random.randi_range(1,5)
			direction = Global.random.randi_range(-1,1)
		next_ground = Global.random.randi_range(0,MAX_JUMP)
		height += direction*next_ground
		
		# Check out of bounds
		if height > max_height:
			height = max_height
		elif height < max_height/2:
			height = max_height/2
		
		# Adds tiles till height
		for y in range(height,max_height):
			Global.map[y][x] = 0
		direction_time -= 1

func _generate_caves():
	# Create auxiliary vars
	var coord = Vector2.ZERO
	var next_direction = Vector2.ZERO
	var next_empty = 0
	var cave_left = cave_space
	
	# Removes x tiles from populated matrix randomly
	# Remove next_empty tiles from position starting in coord, then jump to another position
	while cave_left > 0:
		if next_empty > cave_left:
			next_empty = cave_left
		elif next_empty == 0:
			next_empty = Global.random.randi_range(0,cave_space/10)
			coord = Vector2(Global.random.randi_range(1,max_width-2), Global.random.randi_range(1,max_height/2))
		
		next_direction.x = Global.random.randi_range(-1,1)
		next_direction.y = Global.random.randi_range(-1,1)
		coord = coord+next_direction
		
		# Stops out of bounds
		if coord.x < 0:
			coord.x = 0
		elif coord.x >= max_width:
			coord.x = max_width-1
		if coord.y < 0:
			coord.y = 0
		elif coord.y >= max_height/2:
			coord.y = max_height/2
		
		# Removes tile
		if Global.map[coord.y][coord.x] != 0:
			cave_left -= 1
			next_empty -= 1
			Global.map[coord.y][coord.x] = 0

func _populate_tilemap():
	for x in range(0,max_width):
		for y in range(0,max_height):
			tilemap.set_cell(x,-y,Global.map[y][x]-1)
			tilemap.update_bitmask_area(Vector2(x,-y))
