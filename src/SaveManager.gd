extends Node

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
		print("closing")
		quit_game()

func quit_game():
	save_game()
	get_tree().quit()

func list_saves():
	var dir = Directory.new()
	if !dir.dir_exists("user://Saves"):
		print("Cria saves")
		dir.open("user://")
		dir.make_dir("Saves")
		dir.change_dir("user://Saves")
	else:
		dir.open("user://Saves")
	
	dir.list_dir_begin(true)
	var files = []
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif !file.begins_with("."):
			file = file.rstrip("save")
			file = file.replace(".","")
			files.append(file)
	dir.list_dir_end()
	print(files)
	return files

func save_game():
	if Global.save_file == "":
		return
	var save = File.new()
	print("saving")
	save.open("user://Saves/"+Global.save_file+".save", File.WRITE)
	var save_dict = {
		"tutorial": Global.tutorial,
		"player_level": Global.player_level,
		"player_hearts": Global.player_hearts,
		"coins": Global.coins,
		"xp": Global.xp,
		"player_strenght": Global.player_strenght,
		"max_jumps": Global.max_jumps,
		"map_height": Global.map_height,
		"map": Global.map
	}
	save.store_line(to_json(save_dict))
	save.close()

func load_game():
	var save = File.new()
	if not save.file_exists("user://Saves/"+Global.save_file+".save"):
		return
	
	save.open("user://Saves/"+Global.save_file+".save",File.READ)
	var save_dict = parse_json(save.get_line())
	
	for i in save_dict.keys():
		Global.set(i,save_dict[i])
	
	save.close()

func delete_save(save):
	var dir = Directory.new()
	if !dir.dir_exists("user://Saves"):
		print("Cria saves")
		dir.open("user://")
		dir.make_dir("Saves")
		dir.change_dir("user://Saves")
	else:
		dir.open("user://Saves")
	
	var file = "user://Saves/"+save+".save"
	if dir.file_exists(file):
		print("removing ", file)
		dir.remove(file)
