extends Node

var world = preload("res://Mundo.tscn")
var death = preload("res://Submundo.tscn")
var tutorial = preload("res://UI/Scenes/Tutorial.tscn")
var menu_initial = preload("res://Menu.tscn")

func _ready():
	to_initial()

func to_death():
	_clear_scenes()
	var new = death.instance()
	add_child(new)
	new.connect("life",self,"to_world")

func to_world():
	_clear_scenes()
	var new = world.instance()
	add_child(new)
	new.connect("death",self,"to_death")
	new.connect("menu",self,"to_initial")

func to_tutorial():
	_clear_scenes()
	var new = tutorial.instance()
	add_child(new)
	Global.tutorial = true
	new.connect("end_tutorial",self,"to_world")

func _clear_scenes():
	SaveManager.save_game()
	if get_child_count() > 0:
		get_child(0).call_deferred("free")

func to_initial():
	if get_child_count() > 0:
		get_child(0).call_deferred("free")
	var new = menu_initial.instance()
	add_child(new)
	new.connect("start",self,"start")

func start(load_save):
	if load_save:
		SaveManager.load_game()
	
	if !Global.tutorial:
		to_tutorial()
	else:
		to_world()
