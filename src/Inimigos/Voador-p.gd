extends Enemy

const MAX_SPEED = 20
const GRAVITY = 10

var fly = 0

onready var altura = $Altura

func _physics_process(delta):
	match state:
		IDLE:
			direction = Global.random.randi_range(-1,1)
			fly = Global.random.randi_range(-1,1)
			if altura.get_overlapping_bodies().empty():
				fly = 1
		CHASE:
			_chase()
	_move(delta)

func _move(delta):	
	velocity.x = direction*MAX_SPEED
	velocity.y += GRAVITY*delta
	if fly != 0:
		velocity.y = fly*MAX_SPEED
	
	velocity = move_and_slide(velocity, Vector2(0, -1))

func _chase():
	var player_position = detecta.chase_player()
	if player_position == null:
		state = IDLE
		return
	player_position = (player_position-position).normalized()
	direction = player_position.x
	fly = player_position.y
