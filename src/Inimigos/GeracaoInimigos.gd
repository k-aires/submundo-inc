extends Node2D

signal enemy_death

export(float) var enemy_per_tile = 0.05

var enemies = [preload("res://Inimigos/Voador-p.tscn"),
			preload("res://Inimigos/Saltador-p.tscn")]

var enemy_count = 0
var enemy_position = Vector2.ZERO

func generate_enemies(map_width):
	enemy_count = (map_width*enemy_per_tile)*Global.random.randi_range(1,Global.player_level)
	enemy_position.x = -map_width*8/2
	for x in range(enemy_count):
		var enemy = enemies[Global.random.randi_range(0,enemies.size()-1)].instance()
		add_child(enemy)
		enemy.position = calc_position(map_width)
		enemy.connect("enemy_death",self,"on_enemy_death")

func calc_position(map_width):
	var displacement = int(map_width*8/enemy_count)
	enemy_position.x += displacement
	var new_position = enemy_position
	new_position.x += Global.random.randi_range(-displacement/2,displacement/2)
	return new_position

func on_enemy_death(enemy):
	emit_signal("enemy_death",enemy)
