extends Control

signal start

func _on_Novo_pressed():
	$Menu.visible = false
	$New.visible = true

func _on_Salvo_pressed():
	var button = load("res://UI/Scenes/LoadButton.tscn")
	$Menu.visible = false
	var saves = SaveManager.list_saves()
	for s in saves:
		var new = button.instance()
		$Load.add_child(new)
		new.text = s
		new.connect("press",self,"_load_game")
		new.connect("delete",self,"_delete_game")
	$Load.visible = true

func _load_game(save):
	Global.save_file = save
	emit_signal("start",true)

func _delete_game(save):
	SaveManager.delete_save(save)
	for i in range(1,$Load.get_child_count()):
		$Load.get_child(i).call_deferred("free")
	_on_Salvo_pressed()

func _on_Config_pressed():
	$Menu.visible = false

func _on_Iniciar_pressed():
	var new = $New/HBoxContainer/TextEdit.text
	new = new.replace(".","")
	Global.save_file = new
	emit_signal("start",false)

func _on_Back_pressed():
	$Load.visible = false
	$New.visible = false
	$Menu.visible = true
