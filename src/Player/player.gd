extends KinematicBody2D

signal health_change
signal player_death
signal change_map

const MAX_SPEED = 50
const JUMP = 110
const GRAVITY = 200

var velocity = Vector2.ZERO
var attack = false
var jump = false
var jump_count = 0
var jumping = false

var has_weapon = true
var weapons = {}

onready var weapon = $Arma/Espada
onready var attack_animation = $Sprite/Superior
onready var animation_player = $AnimationPlayer

func _ready():
	for w in $Arma.get_child_count():
		var id = Global.Weapons.get(Global.Weapons.keys()[w])
		weapons[id] = $Arma.get_child(w)
		weapons[id].visible = false
	weapons[Global.Weapons.ESPADA].visible = true

func _process(delta):
	if Input.is_action_pressed("action"):
		var click = get_global_mouse_position()
		emit_signal("change_map",click)

func _physics_process(delta):
	if Global.map_height > 0 && position.y > Global.map_height:
		_on_Hurtbox_area_entered(null)
	
	if Input.is_action_just_pressed("attack"):
		weapon.attack()
		attack_animation.frame = 0
		attack_animation.play("attack")
	
	if velocity.y == 0:
		jump_count = 0
	if (Input.is_action_just_pressed("ui_up")
			&& (jump_count<Global.max_jumps)):
		jump = true
		jump_count += 1
	_move(delta)

func _move(delta):
	var direction = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	if direction > 0:
		velocity.x = MAX_SPEED
	elif direction < 0:
		velocity.x = -MAX_SPEED
	else:
		velocity.x = 0
	
	velocity.y += GRAVITY*delta
	if jump:
		velocity.y = -JUMP
		jump = false
	
	velocity = move_and_slide(velocity, Vector2(0, -1))
	_update_animation(direction)

func _on_Hurtbox_area_entered(area):
	if Global.attack_player():
		emit_signal("player_death")
	emit_signal("health_change")

func _update_animation(direction):
#	weapon.sprite_direction(direction)
	if velocity.y != 0:
		jumping = true
		animation_player.play("jump")
	elif direction > 0:
		weapon.sprite_direction(direction)
		animation_player.play("walk_right")
	elif direction < 0:
		weapon.sprite_direction(direction)
		animation_player.play("walk_left")
	else:
		if jumping:
			animation_player.play("stop")
			jumping = false
		else:
			animation_player.stop(false)

func change_weapon(number):
	pass
