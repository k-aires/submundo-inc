extends Node2D

signal death
signal menu

var map_width = 0

func _ready():
	$Terreno.generate_map()
	$Inimigos.generate_enemies($Terreno.max_width)
	$Inimigos.connect("enemy_death",self,"on_enemy_death")

func on_enemy_death(enemy):
	var effect = load("res://Efeitos/DeathEffect.tscn").instance()
	add_child(effect)
	var category = enemy.category
	effect.position = enemy.position
	enemy.call_deferred("free")
	effect.play(category)
	Global.kill_enemy()
	$UI.coin_update()

func on_player_death():
	var effect = load("res://Efeitos/DeathEffect.tscn").instance()
	add_child(effect)
	effect.connect("effect_end", self, "to_underworld")
	effect.position = $Player.position
	$Player.call_deferred("free")
	effect.play(Global.Category.PLAYER)
	Global.gain_xp()

func to_underworld():
	emit_signal("death")

func _on_UI_menu():
	emit_signal("menu")

func _on_Player_change_map(click):
	$Terreno.clear_tile(click)
