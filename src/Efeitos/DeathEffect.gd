extends Node2D
onready var animation = $AnimationPlayer

signal effect_end

func _ready():
	$Sprite.visible = false

func play(category):
	if !Global.Category.values().has(category):
		animation_end()
		return
	
	match category:
		Global.Category.PEQUENO:
			animation.current_animation = "pequeno"
		Global.Category.PLAYER:
			animation.current_animation = "player"
	
	animation.play()

func animation_end():
	emit_signal("effect_end")
	call_deferred("free")
