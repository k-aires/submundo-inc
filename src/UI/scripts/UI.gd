extends CanvasLayer

signal menu

onready var player_health = $PlayerHealth
onready var coins = $Coins
onready var level = $Level

func _ready():
	level.text = str(Global.player_level)

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		_on_Pause_pressed()

func _on_Player_health_change():
	player_health.health_update()

func coin_update():
	coins.coin_update()

func _on_Pause_pressed():
	if Global.paused:
		Global.paused = false
		$PauseMenu.visible = false
		get_tree().paused = false
	else:
		Global.paused = true
		get_tree().paused = true
		$PauseMenu.visible = true

func _on_PauseMenu_menu():
	get_tree().paused = false
	emit_signal("menu")
