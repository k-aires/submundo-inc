extends Button

signal buy

var price = 0

func set_price(base_price, multiplier = 1):
	price = base_price*multiplier
	$Price.text = str(price)
	can_buy()

func can_buy():
	if price > Global.coins:
		disabled = true

func _on_pressed():
	if Global.coins < price:
		return
	emit_signal("buy", price)
	disabled = true
