extends HBoxContainer

signal press
signal delete

var text = "" setget set_text

func _on_Button_pressed():
	emit_signal("press",text)

func set_text(new):
	text = new
	$Button.text = text

func _on_Delete_pressed():
	emit_signal("delete", text)
