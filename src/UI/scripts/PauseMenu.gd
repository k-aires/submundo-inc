extends Panel

signal menu

func _ready():
	$VBoxContainer/Name.text = Global.save_file

func _on_Save_pressed():
	SaveManager.save_game()

func _on_Menu_pressed():
	emit_signal("menu")

func _on_Quit_pressed():
	SaveManager.quit_game()
