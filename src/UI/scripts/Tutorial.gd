extends Control

signal end_tutorial

var step = 0
var text = ""
var char_count = 0

func _ready():
	next_step()

func next_step():
	match step:
		0:
			text = "Ola! Agora voce faz parte do Submundo!\n"
			text += "O Submundo e um lugar bem simples."
			text += "Aqui, ninguem morre, nao permanentemente."
			set_text()
		1:
			text = "Em vez de morrer, todos no Submundo tem acesso a uma interface."
			text += " Essa interface permite varias melhorias para que "
			text += "todos consigam viver melhor no Submundo."
			set_text()
		2:
			text = "Bom, como alguém novo no Submundo, precisa de algumas dicas essenciais:"
			set_text()
		3:
			text = "Para andar, basta usar as setas do teclado ou "
			text += "A para a esquerda e D para a direita."
			text += " Para pular, basta apertar espaco ou W."
			set_text()
		4:
			text = "O Submundo atualmente esta com um pequeno problema:"
			text += " temos varios monstros andando soltos pelo Submundo."
			set_text()
		5:
			text = "Para lidar com isso, lhe equipamos com uma Espada."
			text += "O ataque e realizado apertando X. Nao se preocupe, voce nao "
			text += "ficara presa no ataque."
			set_text()
		6:
			text = "Ainda nao temos outras armas, mas logo a interface do Submundo "
			text += "vai disponibilizar algumas, ao custo de apenas algumas moedas."
			set_text()
		7:
			text = "Ja que estamos falando de moedas, e bom avisar que as melhorias "
			text += "sao a custo de moedas, que podem ser conseguidas matando monstros."
			set_text()
		8:
			text = "Tenha uma boa estadia!"
			set_text()
		9:
			emit_signal("end_tutorial")

func set_text():
		char_count += 1
		var chars = text.substr(0,char_count)
		$Label.text = chars
		if char_count < text.length():
			$Timer.start()
		else:
			char_count = 0
			step += 1
			$Next.visible = true


func _on_Button_pressed():
	if char_count == 0:
		$Next.visible = false
		next_step()
	else:
		$Timer.stop()
		char_count = text.length()-1
		set_text()
