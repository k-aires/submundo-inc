extends Node

enum Weapons {
	FACA,
	ESPADA,
	GREATSWORD,
	ARCO,
	CAJADO
}

enum Category {
	PLAYER,
	PEQUENO,
	MEDIO,
	GRANDE
}

const MAX_HEARTS = 10
const HEALTH_PER_HEART = 5
const MAX_LEVEL = 10

var tutorial = false

var player_level = 1
var player_hearts = 3
var player_health = player_hearts*HEALTH_PER_HEART
var coins = 0
var kill_count = 0
var xp = 0

var player_strenght = 1
var max_jumps = 1

var map_height = 0
var map = []

var random = RandomNumberGenerator.new()
var paused = false

var save_file = ""

func _ready():
	random.randomize()

func add_heart():
	player_hearts += 1
	if player_hearts < 0:
		player_hearts = 1
	elif player_hearts > MAX_HEARTS:
		player_hearts = MAX_HEARTS

func gain_xp():
	print("kill count: ", kill_count)
	while kill_count > 0:
		var next_level = 10*player_level
		if kill_count+xp < next_level:
			xp += kill_count
			kill_count = 0
		elif kill_count+xp == next_level:
			player_level += 1
			kill_count = 0
			xp = 0
		else:
			player_level += 1
			kill_count = next_level-xp
			xp = 0
	print("xp: ", xp)

func attack_player():
	player_health -= player_level
	if player_health <= 0:
		player_health = 0
		return true
	return false

func kill_enemy():
	kill_count += 1
	coins += player_level

func revive():
	player_health = player_hearts*HEALTH_PER_HEART
